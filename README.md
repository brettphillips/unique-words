# Unique Words

## Setup

This program was run and tested from Python 2.7. Ensure Python 2.7 is installed. If `pip` is in installed, add it as it is Python package manager. 

Once Python and pip are installed, run the following commands to install the necessary dependencies for [TextBlob](https://textblob.readthedocs.io/en/dev/index.html), a wrapper for some NLP from Python.

```python
pip install -U textblob
python -m textblob.download_corpora
```

Once the dependencies are installed, the program can be run with the following command

```python
python uniqueWords.py <optional text file>
```

An optional file can be passed as a parameter to the text, to find unique words from the file. 

The script will output a JSON of the unique words, count and sentence sorted by unique words in alphabetic order.

## Known issues

Using natural language processing felt like the correct way to solve this problem. Issue was choosing between stem and lemma processing. Stem is a quicker way but less robust at determining the actual word origin, so lemma processing was used. This still did not account for an entirely correct result.

The JSON output is not exactly the same as what was the instructions. 