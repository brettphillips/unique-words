import unittest
import uniqueWords

class UniqueWordsTest(unittest.TestCase):

    def testJson(self):
        a = uniqueWords.uniqWds('test', 5) 
        self.assertIsInstance(uniqueWords.wordJson(a), dict)

    def testUniqueClass(self):
        a = uniqueWords.uniqWds('test', 5) 
        self.assertEqual(len(a.sentenceIndex), 0)
        a.addSentance(1)
        self.assertNotEqual(len(a.sentenceIndex), 0)
    
    def testRunningBadFile(self):
        self.assertRaises(IOError, lambda : uniqueWords.main(fileName="fail.txt"))

if __name__ == '__main__':
    unittest.main()