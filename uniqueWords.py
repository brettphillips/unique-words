from textblob import TextBlob, WordList
import json
import sys

''' 
    Class containing the word, count and sentence
    index that will be serialized
'''
class uniqWds:
    def __init__(self, word, count):
        self.word = word
        self.count = count
        self.sentenceIndex = []

    def addSentance(self, senIdx):
        self.sentenceIndex.append(senIdx)

''' 
    Since storing object in a list, need to 
    tell let Python know to turn the object
    to a dictionary 
'''
def wordJson(obj):
    return obj.__dict__

'''
    Perform main object
'''
def main(fileName="hw.txt"):
    # Turn file into string and get all the words in a dict 
    with open(fileName, 'r') as myf:
        data = myf.read().replace('\n', '')
    allWords = TextBlob(data)
    # Get lemma, closest approximate to unique words
    lemma = allWords.words.lemmatize()
    # List of all sentences to find index
    sent = allWords.sentences

    # Loop through set of lemma words, store in object 
    totalWords = []
    for w in set(lemma):
        if w.lower() not in ["a", "the", "and", "of", "in", "be", "also", "as"]:
            uniqueWord = uniqWds(w, lemma.count(w))
            for idx, s in enumerate(sent):
                i = s.find(w)
                if i > -1:
                    uniqueWord.addSentance(idx)
            totalWords.append(uniqueWord)

    # Alphabetize the words
    sortedWords = sorted(totalWords, key=lambda uniqWds: uniqWds.word.lower())
    # Print out array of object
    print json.dumps(sortedWords, default=wordJson)

if __name__ == '__main__':
    if sys.argv[1:]:
        main(fileName=sys.argv[1])
    else: 
        main()